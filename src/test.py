from multiprocessing import Pool
import agent
import problem_gen
from itertools import product
import sys

# Runs a test and prints the results to a file
def run_test(args):
	f = open('results/'+str(args[0])+'-'+str(tests.index(args[1]))+'.txt', 'w')
	sys.stdout = f

	print('World Parameters:', args)

	world1 = problem_gen.generate_world(args[0], *args[1])
	world2 = world1.clone()
	world1.print_grid()

	# run the reflex agent
	reflexAgent = agent.ReflexAgent(world1)
	reflexAgent.run()
	reflexResults = (world1.explorer.payoff, world1.won, world1.explorer.kills, world1.explorer.deaths['wumpus'], world1.explorer.deaths['pit'])

	# run the logic agent
	logicAgent = agent.WumpusAgent(world2)
	logicAgent.run()
	logicResults = (world2.explorer.payoff, world2.won, world2.explorer.kills, world2.explorer.deaths['wumpus'], world2.explorer.deaths['pit'])

	print()
	print('Reflex:')
	print('\tScore:', reflexResults[0])
	print('\tGold Found: ', reflexResults[1])
	print('\tWumpuses Killed: ', reflexResults[2])
	print('\tDeaths by Wumpus: ', reflexResults[3])
	print('\tDeaths by Pit: ', reflexResults[4])
	print('Logical:')
	print('\tScore:', logicResults[0])
	print('\tGold Found: ', logicResults[1])
	print('\tWumpuses Killed: ', logicResults[2])
	print('\tDeaths by Wumpus: ', logicResults[3])
	print('\tDeaths by Pit: ', logicResults[4])
	print()
	print()

if __name__ == '__main__':
	# problem gen parameters
	tests = [
		(0, 0, 0),
		(0.1, 0.1, 0.1),
		(0.2, 0.2, 0.2),
		(0.3, 0.3, 0.3),
		(0.1, 0.4, 0.4),
		(0.4, 0.1, 0.4),
		(0.4, 0.4, 0.1),
		(0.1, 0.1, 0.6),
		(0.1, 0.6, 0.1),
		(0.6, 0.1, 0.1),
	]

	# grab a pool of processors
	pool = Pool(4)
	# Run the tests
	pool.map(run_test, product(range(5,30,5), tests), 1)

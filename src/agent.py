from problem_gen import *
from logic import *
from itertools import product
import random

class ReflexAgent():
	def __init__(self, world):
		self.world = world

	# Three percept-action maps
	def tell_axioms(self, kb):
		kb.tell(Clause(
			Predicate('Bump', (), negated=True),
			Predicate('Action', (TURN_LEFT,))
		))

		kb.tell(Clause(
			Predicate('Gold', (), negated=True),
			Predicate('Action', (GRAB,))
		))

		kb.tell(Clause(
			Predicate('Bump', ()),
			Predicate('Gold', ()),
			Predicate('Action', (MOVE,))
		))

	# main loop
	def run(self):
		# While we're not finished and it hasn't been too long
		while not self.world.finished and self.world.explorer.payoff > -10000:
			# new knowledge base every time step
			kb = KnowledgeBase()
			self.tell_axioms(kb)

			# tell percepts
			smell, wind, glitter, scream, bump = self.world.get_percepts()

			kb.tell(Predicate('Smell', (), negated=(not smell)))
			kb.tell(Predicate('Wind', (), negated=(not wind)))
			kb.tell(Predicate('Gold', (), negated=(not glitter)))
			kb.tell(Predicate('Scream', (), negated=(not scream)))
			kb.tell(Predicate('Bump', (), negated=(not bump)))

			# ask for an action
			try:
				if kb.ask(Predicate('Action', (MOVE,))):
					self.world.move_explorer()
				elif kb.ask(Predicate('Action', (TURN_LEFT,))):
					self.world.turn_explorer_left()
				elif kb.ask(Predicate('Action', (TURN_RIGHT,))):
					self.world.turn_explorer_right()
				elif kb.ask(Predicate('Action', (SHOOT,))):
					self.world.shoot_arrow()
				elif kb.ask(Predicate('Action', (GRAB,))):
					self.world.pick_up_gold()
				else:
					#print(smell, wind, glitter, scream, bump)
					#print('Explorer did not have an action for this...')
					return False

			# Keep going on death
			except DeathException as de:
				#print('Death:', de)
				continue
			except RuntimeError:
				continue
		if self.world.finished:
			#print('Victory! Found the gold with a score of', self.world.explorer.payoff)
			return True
		else:
			#print('Failure... The explorer wandered that dark cavern for all eternity')
			return False

class WumpusAgent():
	def __init__(self, world):
		self.world = world
		self.x, self.y = world.explorer.location
		if world.explorer.direction == (0, 1):
			self.dir = NORTH
		elif world.explorer.direction == (1, 0):
			self.dir = EAST
		elif world.explorer.direction == (0, -1):
			self.dir = SOUTH
		elif world.explorer.direction == (-1, 0):
			self.dir = WEST
		self.arrows = world.explorer.arrows
		self.kb = KnowledgeBase()
		self.tell_axioms()

	# Some rules about pit/wumpus/safe relationships
	def tell_axioms(self):
		self.kb.tell(Clause(
			Predicate('Safe', ('x', 'y')),
			Predicate('Pit', ('x', 'y')),
			Predicate('Wumpus', ('x', 'y'))
		))

		self.kb.tell(Clause(
			Predicate('Safe', ('x', 'y'), negated=True),
			Predicate('Wumpus', ('x', 'y'), negated=True)
		))
		self.kb.tell(Clause(
			Predicate('Safe', ('x', 'y'), negated=True),
			Predicate('Pit', ('x', 'y'), negated=True)
		))
		self.kb.tell(Clause(
			Predicate('Wumpus', ('x', 'y'), negated=True),
			Predicate('Pit', ('x', 'y'), negated=True)
		))

		self.kb.tell(Clause(
			Predicate('Obstacle', ('x', 'y'), negated=True),
			Predicate('Safe', ('x', 'y')),
		))

	# main loop
	def run(self):
		# Start with everything but current location unexplored
		for x, y in product(range(self.world.size), range(self.world.size)):
			if (x, y) == (self.x, self.y):
				pass
			else:
				self.kb.tell(Predicate('Explored', (x, y), negated=True))

		# while we're not done and it hasn't been too long
		while not self.world.finished and self.world.explorer.payoff > -10000:
			smell, wind, glitter, scream, bump = self.world.get_percepts()

			newKB = KnowledgeBase()
			# Transfer some facts over
			for x, y in product(range(self.world.size), range(self.world.size)):
				# If it was safe, it is still safe
				if self.kb.subsumes(Predicate('Safe', (x,y))):
					newKB.tell(Predicate('Safe', (x,y)))
				# if it was not safe and there hasn't been a scream, it's still not safe
				elif not scream and self.kb.subsumes(Predicate('Safe', (x,y), negated=True)):
					newKB.tell(Predicate('Safe', (x,y), negated=True))

				# If there was a pit, there's still a pit
				if self.kb.subsumes(Predicate('Pit', (x,y))):
					newKB.tell(Predicate('Pit', (x,y)))
				elif self.kb.subsumes(Predicate('Pit', (x,y), negated=True)):
					newKB.tell(Predicate('Pit', (x,y), negated=True))

				# if there was wind, there's still wind
				if self.kb.subsumes(Predicate('Wind', (x,y))):
					newKB.tell(Predicate('Wind', (x,y)))
				elif self.kb.subsumes(Predicate('Wind', (x,y), negated=True)):
					newKB.tell(Predicate('Wind', (x,y), negated=True))

				# If there was a wumpus and there wasn't a scream, there's still a wumpus
				if not scream and self.kb.subsumes(Predicate('Wumpus', (x,y))):
					newKB.tell(Predicate('Wumpus', (x,y)))
				# If there was wasn't a wumpus, there's still not a wumpus
				elif self.kb.subsumes(Predicate('Wumpus', (x,y), negated=True)):
					newKB.tell(Predicate('Wumpus', (x,y), negated=True))

				# If there was a smell and there wasn't a scream, there's still a smell
				if not scream and self.kb.subsumes(Predicate('Smell', (x,y))):
					newKB.tell(Predicate('Smell', (x,y)))
				# If there wasn't a smell, there still isn't a smell
				elif self.kb.subsumes(Predicate('Smell', (x,y), negated=True)):
					newKB.tell(Predicate('Smell', (x,y), negated=True))

				# If it was explored, it's still explored
				if self.kb.subsumes(Predicate('Explored', (x,y))):
					newKB.tell(Predicate('Explored', (x,y)))
				# If it wasn't explored and we're not there now, it's still not explored
				if (x, y) != (self.x, self.y) and self.kb.subsumes(Predicate('Explored', (x,y), negated=True)):
					newKB.tell(Predicate('Explored', (x,y), negated=True))

				# if there was or wasn't a pit, there still is/isn't
				if self.kb.subsumes(Predicate('Obstacle', (x,y))):
					newKB.tell(Predicate('Obstacle', (x,y)))
				elif self.kb.subsumes(Predicate('Obstacle', (x,y), negated=True)):
					newKB.tell(Predicate('Obstacle', (x,y), negated=True))
			
			self.kb = newKB
			self.tell_axioms() # tell rules again

			# detect obstacle
			if bump:
				self.kb.tell(Predicate('Obstacle', get_adj(self.x, self.y, self.dir)))
			else:
				self.kb.tell(Predicate('Obstacle', (self.x, self.y), negated=True))

			# Tell some info
			self.kb.tell(Predicate('Location', (self.x, self.y)))
			self.kb.tell(Predicate('Explored', (self.x, self.y)))
			self.kb.tell(Predicate('Safe', (self.x, self.y)))
			self.kb.tell(Predicate('Smell', (self.x, self.y), negated=(not smell)))
			self.kb.tell(Predicate('Wind', (self.x, self.y), negated=(not wind)))
			self.kb.tell(Predicate('Gold', (self.x, self.y), negated=(not glitter)))
			self.kb.tell(Predicate('Scream', (), negated=(not scream)))
			self.kb.tell(Predicate('Bump', (), negated=(not bump)))

			#print('Location: ({}, {})'.format(self.x, self.y))
			#print('Direction: {}'.format(self.dir))
			#print('Percepts: S:{} W:{} G:{} C:{} B:{}'.format(smell, wind, glitter, scream, bump))
			#self.print_map()

			# actions
			try:
				self.strategy() # execute the strategy
			except DeathException as de: # Death is not the end - Bob Dylan
				#print('Death:', de)
				if de.fate == 'WUMPUS':
					self.kb.tell(Predicate('Wumpus', de.location))
				elif de.fate == 'PIT':
					self.kb.tell(Predicate('Pit', de.location))
				else:
					return

	def strategy(self):
		# If the gold is here, grab it
		if self.kb.ask(Predicate('Gold', (self.x, self.y))):
			self.take_action(GRAB)
			return

		# move an to adjacent unexplored safe cell
		for d in [NORTH, EAST, SOUTH, WEST]:
			adj = get_adj(self.x, self.y, d)
			if not self.is_obstacle(*adj) and self.is_safe(*adj) and not self.is_explored(*adj):
				self.turn_to(d)
				self.take_action(MOVE)
				return

		# shoot at an adjacent confirmed wumpus, if we have arrows
		if self.arrows > 0:
			for d in [NORTH, EAST, SOUTH, WEST]:
				adj = get_adj(self.x, self.y, d)
				if self.is_wumpus(*adj):
					self.turn_to(d)
					self.take_action(SHOOT)
					return

		# explore an adjacent unsafe square
		for d in [NORTH, EAST, SOUTH, WEST]:
			adj = get_adj(self.x, self.y, d)
			if not self.is_obstacle(*adj) and not self.is_explored(*adj) and not self.is_wumpus(*adj) and not self.is_pit(*adj):
				self.turn_to(d)
				self.take_action(MOVE)
				return

		# random walk
		dirs = [NORTH, EAST, SOUTH, WEST]
		random.shuffle(dirs)
		for rd in dirs:
			adj = get_adj(self.x, self.y, rd)
			if not self.is_obstacle(*adj) and not self.is_wumpus(*adj) and not self.is_pit(*adj):
				self.turn_to(rd)
				self.take_action(MOVE)
				return

		# Somehow all of the above failed
		self.take_action(GIVE_UP)

	# Is (x,y) smelly?
	def is_smelly(self, x, y):
		if not self.world.in_bounds(x, y):
			return None

		if self.kb.ask(Predicate('Smell', (x, y))):
			return True
		elif self.kb.ask(Predicate('Smell', (x, y), negated=True)):
			return False

		return None

	# Is (x,y) windy?
	def is_windy(self, x, y):
		if not self.world.in_bounds(x, y):
			return None

		if self.kb.ask(Predicate('Wind', (x, y))):
			return True
		elif self.kb.ask(Predicate('Wind', (x, y), negated=True)):
			return False

		return None

	# Is (x,y) safe?
	def is_safe(self, x, y):
		if not self.world.in_bounds(x, y):
			return None

		if self.kb.ask(Predicate('Safe', (x, y))):
			return True
		elif self.kb.ask(Predicate('Safe', (x, y), negated=True)):
			return False

		# check adjacent for smells/winds
		n = get_adj(x, y, NORTH)
		e = get_adj(x, y, EAST)
		s = get_adj(x, y, SOUTH)
		w = get_adj(x, y, WEST)
		if self.is_smelly(*n) == False and self.is_windy(*n) == False:
			self.kb.tell(Predicate('Safe', (x, y)))
			return True
		if self.is_smelly(*e) == False and self.is_windy(*e) == False:
			self.kb.tell(Predicate('Safe', (x, y)))
			return True
		if self.is_smelly(*s) == False and self.is_windy(*s) == False:
			self.kb.tell(Predicate('Safe', (x, y)))
			return True
		if self.is_smelly(*w) == False and self.is_windy(*w) == False:
			self.kb.tell(Predicate('Safe', (x, y)))
			return True

		return None

	# non-recursive check for wumpi
	def is_wumpus_simple(self, x, y):
		if not self.world.in_bounds(x, y):
			return False

		if self.kb.ask(Predicate('Wumpus', (x, y))):
			return True
		elif self.kb.ask(Predicate('Wumpus', (x, y), negated=True)):
			return False

		n = get_adj(x, y, NORTH)
		e = get_adj(x, y, EAST)
		s = get_adj(x, y, SOUTH)
		w = get_adj(x, y, WEST)
		if self.is_smelly(*n) == True and (self.is_smelly(*e) == False or self.is_smelly(*s) == False or self.is_smelly(*w) == False):
			self.kb.tell(Predicate('Wumpus', (x, y), negated=True))
			return False
		if self.is_smelly(*e) == True and (self.is_smelly(*n) == False or self.is_smelly(*s) == False or self.is_smelly(*w) == False):
			self.kb.tell(Predicate('Wumpus', (x, y), negated=True))
			return False
		if self.is_smelly(*s) == True and (self.is_smelly(*n) == False or self.is_smelly(*e) == False or self.is_smelly(*w) == False):
			self.kb.tell(Predicate('Wumpus', (x, y), negated=True))
			return False
		if self.is_smelly(*w) == True and (self.is_smelly(*n) == False or self.is_smelly(*e) == False or self.is_smelly(*s) == False):
			self.kb.tell(Predicate('Wumpus', (x, y), negated=True))
			return False

		return None

	# more sofphisticated check for wumpi
	def is_wumpus(self, x, y):
		if not self.world.in_bounds(x, y):
			return False

		if self.kb.ask(Predicate('Wumpus', (x, y))):
			return True
		elif self.kb.ask(Predicate('Wumpus', (x, y), negated=True)):
			return False

		n = get_adj(x, y, NORTH)
		e = get_adj(x, y, EAST)
		s = get_adj(x, y, SOUTH)
		w = get_adj(x, y, WEST)

		if self.is_smelly(*n) == True and (self.is_smelly(*e) == False or self.is_smelly(*s) == False or self.is_smelly(*w) == False):
			self.kb.tell(Predicate('Wumpus', (x, y), negated=True))
			return False
		if self.is_smelly(*e) == True and (self.is_smelly(*n) == False or self.is_smelly(*s) == False or self.is_smelly(*w) == False):
			self.kb.tell(Predicate('Wumpus', (x, y), negated=True))
			return False
		if self.is_smelly(*s) == True and (self.is_smelly(*n) == False or self.is_smelly(*e) == False or self.is_smelly(*w) == False):
			self.kb.tell(Predicate('Wumpus', (x, y), negated=True))
			return False
		if self.is_smelly(*w) == True and (self.is_smelly(*n) == False or self.is_smelly(*e) == False or self.is_smelly(*s) == False):
			self.kb.tell(Predicate('Wumpus', (x, y), negated=True))
			return False

		if self.is_smelly(*n) == True and self.is_wumpus_simple(*get_adj(*n, NORTH)) == False and self.is_wumpus_simple(*get_adj(*n, EAST)) == False and self.is_wumpus_simple(*get_adj(*n, WEST)) == False:
			self.kb.tell(Predicate('Wumpus', (x, y)))
			return True
		if self.is_smelly(*e) == True and self.is_wumpus_simple(*get_adj(*e, EAST)) == False and self.is_wumpus_simple(*get_adj(*e, SOUTH)) == False and self.is_wumpus_simple(*get_adj(*e, NORTH)) == False:
			self.kb.tell(Predicate('Wumpus', (x, y)))
			return True
		if self.is_smelly(*s) == True and self.is_wumpus_simple(*get_adj(*s, SOUTH)) == False and self.is_wumpus_simple(*get_adj(*s, WEST)) == False and self.is_wumpus_simple(*get_adj(*s, EAST)) == False:
			self.kb.tell(Predicate('Wumpus', (x, y)))
			return True
		if self.is_smelly(*w) == True and self.is_wumpus_simple(*get_adj(*w, WEST)) == False and self.is_wumpus_simple(*get_adj(*w, NORTH)) == False and self.is_wumpus_simple(*get_adj(*w, SOUTH)) == False:
			self.kb.tell(Predicate('Wumpus', (x, y)))
			return True

		return None

	# non-recursive check for pits
	def is_pit_simple(self, x, y):
		if not self.world.in_bounds(x, y):
			return False

		if self.kb.ask(Predicate('Pit', (x, y))):
			return True
		elif self.kb.ask(Predicate('Pit', (x, y), negated=True)):
			return False

		n = get_adj(x, y, NORTH)
		e = get_adj(x, y, EAST)
		s = get_adj(x, y, SOUTH)
		w = get_adj(x, y, WEST)

		if self.is_windy(*n) == True and (self.is_windy(*e) == False or self.is_windy(*s) == False or self.is_windy(*w) == False):
			self.kb.tell(Predicate('Pit', (x, y), negated=True))
			return False
		if self.is_windy(*e) == True and (self.is_windy(*n) == False or self.is_windy(*s) == False or self.is_windy(*w) == False):
			self.kb.tell(Predicate('Pit', (x, y), negated=True))
			return False
		if self.is_windy(*s) == True and (self.is_windy(*n) == False or self.is_windy(*e) == False or self.is_windy(*w) == False):
			self.kb.tell(Predicate('Pit', (x, y), negated=True))
			return False
		if self.is_windy(*w) == True and (self.is_windy(*n) == False or self.is_windy(*e) == False or self.is_windy(*s) == False):
			self.kb.tell(Predicate('Pit', (x, y), negated=True))
			return False

		return None

	# more sophisticated check for pits
	def is_pit(self, x, y):
		if not self.world.in_bounds(x, y):
			return False

		if self.kb.ask(Predicate('Pit', (x, y))):
			return True
		elif self.kb.ask(Predicate('Pit', (x, y), negated=True)):
			return False

		n = get_adj(x, y, NORTH)
		e = get_adj(x, y, EAST)
		s = get_adj(x, y, SOUTH)
		w = get_adj(x, y, WEST)

		if self.is_windy(*n) == True and (self.is_windy(*e) == False or self.is_windy(*s) == False or self.is_windy(*w) == False):
			self.kb.tell(Predicate('Pit', (x, y), negated=True))
			return False
		if self.is_windy(*e) == True and (self.is_windy(*n) == False or self.is_windy(*s) == False or self.is_windy(*w) == False):
			self.kb.tell(Predicate('Pit', (x, y), negated=True))
			return False
		if self.is_windy(*s) == True and (self.is_windy(*n) == False or self.is_windy(*e) == False or self.is_windy(*w) == False):
			self.kb.tell(Predicate('Pit', (x, y), negated=True))
			return False
		if self.is_windy(*w) == True and (self.is_windy(*n) == False or self.is_windy(*e) == False or self.is_windy(*s) == False):
			self.kb.tell(Predicate('Pit', (x, y), negated=True))
			return False

		if self.is_windy(*n) == True and self.is_pit_simple(*get_adj(*n, NORTH)) == False and self.is_pit_simple(*get_adj(*n, EAST)) == False and self.is_pit_simple(*get_adj(*n, WEST)) == False:
			self.kb.tell(Predicate('Pit', (x, y)))
			return True
		if self.is_windy(*e) == True and self.is_pit_simple(*get_adj(*e, EAST)) == False and self.is_pit_simple(*get_adj(*e, SOUTH)) == False and self.is_pit_simple(*get_adj(*e, NORTH)) == False:
			self.kb.tell(Predicate('Pit', (x, y)))
			return True
		if self.is_windy(*s) == True and self.is_pit_simple(*get_adj(*s, SOUTH)) == False and self.is_pit_simple(*get_adj(*s, WEST)) == False and self.is_pit_simple(*get_adj(*s, EAST)) == False:
			self.kb.tell(Predicate('Pit', (x, y)))
			return True
		if self.is_windy(*w) == True and self.is_pit_simple(*get_adj(*w, WEST)) == False and self.is_pit_simple(*get_adj(*w, NORTH)) == False and self.is_pit_simple(*get_adj(*w, SOUTH)) == False:
			self.kb.tell(Predicate('Pit', (x, y)))
			return True

		return None

	# is (x,y) and obstacle?
	def is_obstacle(self, x, y):
		if not self.world.in_bounds(x, y):
			return True

		if self.kb.ask(Predicate('Obstacle', (x, y))):
			return True
		elif self.kb.ask(Predicate('Obstacle', (x, y), negated=True)):
			return False

		return None

	# Has (x,y) been explored?
	def is_explored(self, x, y):
		if not self.world.in_bounds(x, y):
			return True

		if self.kb.ask(Predicate('Explored', (x, y))):
			return True
		elif self.kb.ask(Predicate('Explored', (x, y), negated=True)):
			return False

	# For debug purposes
	def print_map(self):
		safe = {}
		wumpus = {}
		pit = {}
		obs = {}
		explored = {}
		for y, x in product(reversed(range(self.world.size)), range(self.world.size)):
			safe[(x,y)] = self.is_safe(x, y)
			wumpus[(x,y)] = self.is_wumpus(x, y)
			pit[(x,y)] = self.is_pit(x, y)
			obs[(x,y)] = self.is_obstacle(x, y)
			explored[(x,y)] = self.is_explored(x, y)
		for y, x in product(reversed(range(self.world.size)), range(self.world.size)):
			if (x, y) == (self.x, self.y):
				if self.dir == NORTH:
					print('^ ', end='')
				elif self.dir == EAST:
					print('> ', end='')
				elif self.dir == SOUTH:
					print('v ', end='')
				if self.dir == WEST:
					print('< ', end='')
			elif explored[(x,y)] == True:
				print('* ', end='')
			elif obs[(x,y)] == True:
				print('X ', end='')
			elif safe[(x,y)] == True:
				print('- ', end='')
			elif wumpus[(x,y)] == True:
				print('W ', end='')
			elif pit[(x,y)] == True:
				print('P ', end='')
			else:
				print('? ', end='')

			if x == self.world.size-1:
				print()

	# helper method to turn to a target direction
	def turn_to(self, target):
		if target == self.dir:
			return
		elif target == (self.dir % 4) + 1:
			self.take_action(TURN_RIGHT)
		else:
			self.take_action(TURN_LEFT)
			self.turn_to(target)

	# Takes a direction and updates our state
	def take_action(self, action):
		if action == MOVE:
			#print('MOVE')
			if not self.world.move_explorer():
				if self.dir == NORTH:
					self.y += 1
				elif self.dir == EAST:
					self.x += 1
				elif self.dir == SOUTH:
					self.y -= 1
				elif self.dir == WEST:
					self.x -= 1
		elif action == TURN_LEFT:
			#print('TURN_LEFT')
			self.world.turn_explorer_left()
			self.dir -= 1
			if self.dir == 0: self.dir = WEST
		elif action == TURN_RIGHT:
			#print('TURN_RIGHT')
			self.world.turn_explorer_right()
			self.dir = (self.dir % 4) + 1
		elif action == SHOOT:
			#print('SHOOT')
			try:
				self.world.shoot_arrow()
				self.arrows -= 1
			except:
				pass
		elif action == GRAB:
			#print('GRAB')
			self.world.pick_up_gold()
		elif action == GIVE_UP:
			self.world.give_up()
		else:
			raise ValueError('invalid action')

# Get the coordinates of adjacent square in direction d
def get_adj(x, y, d):
	if d == NORTH:
		return (x, y+1)
	elif d == EAST:
		return (x+1, y)
	elif d == SOUTH:
		return (x, y-1)
	elif d == WEST:
		return (x-1, y)

### named constants

NORTH = 1
EAST  = 2
SOUTH = 3
WEST  = 4


MOVE = 10
TURN_LEFT = 11
TURN_RIGHT = 12
SHOOT = 13
GRAB = 14
GIVE_UP = 15

# run the logical agent
def logical(world=None):
	if world is None:
		world = generate_world(5, 0.1, 0.1, 0.1)
	agent = WumpusAgent(world)
	agent.run()
	#print(world.explorer.payoff)
	return world.explorer.payoff

# run the reflex agent
def reflex(world=None):
	if world is None:
		world = generate_world(5, 0.1, 0.1, 0.1)
	agent = ReflexAgent(world)
	agent.run()
	#print(world.explorer.payoff)
	return world.explorer.payoff

if __name__ == '__main__':
	logical()
	# for i in range(10):
	# 	#print('Run {}:'.format(i))
	# 	reflex()
	# 	#print()
from problem_gen import *
from logic import *
from itertools import product

class ManualAgent():
	def __init__(self, world):
		self.world = world
		self.x, self.y = world.explorer.location
		if world.explorer.direction == (0, 1):
			self.dir = NORTH
		elif world.explorer.direction == (1, 0):
			self.dir = EAST
		elif world.explorer.direction == (0, -1):
			self.dir = SOUTH
		elif world.explorer.direction == (-1, 0):
			self.dir = WEST
		self.arrows = world.explorer.arrows
		self.kb = KnowledgeBase()
		self.tell_axioms()

	def tell_axioms(self):
		self.kb.tell(Clause(
			Predicate('Safe', ('x', 'y')),
			Predicate('Pit', ('x', 'y')),
			Predicate('Wumpus', ('x', 'y'))
		))

		self.kb.tell(Clause(
			Predicate('Safe', ('x', 'y'), negated=True),
			Predicate('Wumpus', ('x', 'y'), negated=True)
		))
		self.kb.tell(Clause(
			Predicate('Safe', ('x', 'y'), negated=True),
			Predicate('Pit', ('x', 'y'), negated=True)
		))
		self.kb.tell(Clause(
			Predicate('Wumpus', ('x', 'y'), negated=True),
			Predicate('Pit', ('x', 'y'), negated=True)
		))

	def run(self):
		for x, y in product(range(self.world.size), range(self.world.size)):
			if (x, y) == (self.x, self.y):
				pass
			else:
				self.kb.tell(Predicate('Explored', (x, y), negated=True))

		while not self.world.finished:
			smell, wind, glitter, scream, bump = self.world.get_percepts()

			newKB = KnowledgeBase()
			for x, y in product(range(self.world.size), range(self.world.size)):
				if self.kb.subsumes(Predicate('Safe', (x,y))):
					newKB.tell(Predicate('Safe', (x,y)))
				elif not scream and self.kb.subsumes(Predicate('Safe', (x,y), negated=True)):
					newKB.tell(Predicate('Safe', (x,y), negated=True))

				if self.kb.subsumes(Predicate('Pit', (x,y))):
					newKB.tell(Predicate('Pit', (x,y)))
				elif self.kb.subsumes(Predicate('Pit', (x,y), negated=True)):
					newKB.tell(Predicate('Pit', (x,y), negated=True))

				if self.kb.subsumes(Predicate('Wind', (x,y))):
					newKB.tell(Predicate('Wind', (x,y)))
				elif self.kb.subsumes(Predicate('Wind', (x,y), negated=True)):
					newKB.tell(Predicate('Wind', (x,y), negated=True))

				if not scream and self.kb.subsumes(Predicate('Wumpus', (x,y))):
					newKB.tell(Predicate('Wumpus', (x,y)))
				elif self.kb.subsumes(Predicate('Wumpus', (x,y), negated=True)):
					newKB.tell(Predicate('Wumpus', (x,y), negated=True))

				if not scream and self.kb.subsumes(Predicate('Smell', (x,y))):
					newKB.tell(Predicate('Smell', (x,y)))
				elif self.kb.subsumes(Predicate('Smell', (x,y), negated=True)):
					newKB.tell(Predicate('Smell', (x,y), negated=True))

				if self.kb.subsumes(Predicate('Explored', (x,y))):
					newKB.tell(Predicate('Explored', (x,y)))
				if (x, y) != (self.x, self.y) and self.kb.subsumes(Predicate('Explored', (x,y), negated=True)):
					newKB.tell(Predicate('Explored', (x,y), negated=True))

				if self.kb.subsumes(Predicate('Obstacle', (x,y))):
					newKB.tell(Predicate('Obstacle', (x,y)))
				elif self.kb.subsumes(Predicate('Obstacle', (x,y), negated=True)):
					newKB.tell(Predicate('Obstacle', (x,y), negated=True))
			
			self.kb = newKB
			self.tell_axioms()

			if bump:
				self.kb.tell(Predicate('Obstacle', get_adj(self.x, self.y, self.dir)))
			else:
				self.kb.tell(Predicate('Obstacle', (self.x, self.y), negated=False))

			self.kb.tell(Predicate('Location', (self.x, self.y)))
			self.kb.tell(Predicate('Explored', (self.x, self.y)))
			self.kb.tell(Predicate('Safe', (self.x, self.y)))
			self.kb.tell(Predicate('Smell', (self.x, self.y), negated=(not smell)))
			self.kb.tell(Predicate('Wind', (self.x, self.y), negated=(not wind)))
			self.kb.tell(Predicate('Gold', (self.x, self.y), negated=(not glitter)))
			self.kb.tell(Predicate('Scream', (), negated=(not scream)))
			self.kb.tell(Predicate('Bump', (), negated=(not bump)))

			print('Location: ({}, {})'.format(self.x, self.y))
			print('True Location: {}'.format(self.world.explorer.location))
			print('Direction: {}'.format(self.dir))
			print('Percepts: S:{} W:{} G:{} C:{} B:{}'.format(smell, wind, glitter, scream, bump))
			self.print_map()
			inp = input('Action: ').upper()
			action = globals()[inp]
			self.take_action(action)

	def is_smelly(self, x, y):
		if not self.world.in_bounds(x, y):
			return None

		if self.kb.ask(Predicate('Smell', (x, y))):
			return True
		elif self.kb.ask(Predicate('Smell', (x, y), negated=True)):
			return False

		return None

	def is_windy(self, x, y):
		if not self.world.in_bounds(x, y):
			return None

		if self.kb.ask(Predicate('Wind', (x, y))):
			return True
		elif self.kb.ask(Predicate('Wind', (x, y), negated=True)):
			return False

		return None

	def is_safe(self, x, y):
		if not self.world.in_bounds(x, y):
			return None

		if self.kb.ask(Predicate('Safe', (x, y))):
			return True
		elif self.kb.ask(Predicate('Safe', (x, y), negated=True)):
			return False

		n = get_adj(x, y, NORTH)
		e = get_adj(x, y, EAST)
		s = get_adj(x, y, SOUTH)
		w = get_adj(x, y, WEST)
		if self.is_smelly(*n) == False and self.is_windy(*n) == False:
			self.kb.tell(Predicate('Safe', (x, y)))
			return True
		if self.is_smelly(*e) == False and self.is_windy(*e) == False:
			self.kb.tell(Predicate('Safe', (x, y)))
			return True
		if self.is_smelly(*s) == False and self.is_windy(*s) == False:
			self.kb.tell(Predicate('Safe', (x, y)))
			return True
		if self.is_smelly(*w) == False and self.is_windy(*w) == False:
			self.kb.tell(Predicate('Safe', (x, y)))
			return True

		return None

	def is_wumpus_simple(self, x, y):
		if not self.world.in_bounds(x, y):
			return False

		if self.kb.ask(Predicate('Wumpus', (x, y))):
			return True
		elif self.kb.ask(Predicate('Wumpus', (x, y), negated=True)):
			return False

		return None

	def is_wumpus(self, x, y):
		if not self.world.in_bounds(x, y):
			return False

		if self.kb.ask(Predicate('Wumpus', (x, y))):
			return True
		elif self.kb.ask(Predicate('Wumpus', (x, y), negated=True)):
			return False

		n = get_adj(x, y, NORTH)
		e = get_adj(x, y, EAST)
		s = get_adj(x, y, SOUTH)
		w = get_adj(x, y, WEST)
		if self.is_smelly(*n) == True and self.is_wumpus_simple(*get_adj(*n, NORTH)) == False and self.is_wumpus_simple(*get_adj(*n, EAST)) == False and self.is_wumpus_simple(*get_adj(*n, WEST)) == False:
			self.kb.tell(Predicate('Wumpus', (x, y)))
			return True
		if self.is_smelly(*e) == True and self.is_wumpus_simple(*get_adj(*e, EAST)) == False and self.is_wumpus_simple(*get_adj(*e, SOUTH)) == False and self.is_wumpus_simple(*get_adj(*e, NORTH)) == False:
			self.kb.tell(Predicate('Wumpus', (x, y)))
			return True
		if self.is_smelly(*s) == True and self.is_wumpus_simple(*get_adj(*s, SOUTH)) == False and self.is_wumpus_simple(*get_adj(*s, WEST)) == False and self.is_wumpus_simple(*get_adj(*s, EAST)) == False:
			self.kb.tell(Predicate('Wumpus', (x, y)))
			return True
		if self.is_smelly(*w) == True and self.is_wumpus_simple(*get_adj(*w, WEST)) == False and self.is_wumpus_simple(*get_adj(*w, NORTH)) == False and self.is_wumpus_simple(*get_adj(*w, SOUTH)) == False:
			self.kb.tell(Predicate('Wumpus', (x, y)))
			return True

		return None

	def is_pit_simple(self, x, y):
		if not self.world.in_bounds(x, y):
			return False

		if self.kb.ask(Predicate('Pit', (x, y))):
			return True
		elif self.kb.ask(Predicate('Pit', (x, y), negated=True)):
			return False

		return None

	def is_pit(self, x, y):
		if not self.world.in_bounds(x, y):
			return False

		if self.kb.ask(Predicate('Pit', (x, y))):
			return True
		elif self.kb.ask(Predicate('Pit', (x, y), negated=True)):
			return False

		n = get_adj(x, y, NORTH)
		e = get_adj(x, y, EAST)
		s = get_adj(x, y, SOUTH)
		w = get_adj(x, y, WEST)
		if self.is_windy(*n) == True and self.is_pit_simple(*get_adj(*n, NORTH)) == False and self.is_pit_simple(*get_adj(*n, EAST)) == False and self.is_pit_simple(*get_adj(*n, WEST)) == False:
			self.kb.tell(Predicate('Pit', (x, y)))
			return True
		if self.is_windy(*e) == True and self.is_pit_simple(*get_adj(*e, EAST)) == False and self.is_pit_simple(*get_adj(*e, SOUTH)) == False and self.is_pit_simple(*get_adj(*e, NORTH)) == False:
			self.kb.tell(Predicate('Pit', (x, y)))
			return True
		if self.is_windy(*s) == True and self.is_pit_simple(*get_adj(*s, SOUTH)) == False and self.is_pit_simple(*get_adj(*s, WEST)) == False and self.is_pit_simple(*get_adj(*s, EAST)) == False:
			self.kb.tell(Predicate('Pit', (x, y)))
			return True
		if self.is_windy(*w) == True and self.is_pit_simple(*get_adj(*w, WEST)) == False and self.is_pit_simple(*get_adj(*w, NORTH)) == False and self.is_pit_simple(*get_adj(*w, SOUTH)) == False:
			self.kb.tell(Predicate('Pit', (x, y)))
			return True

		return None

	def is_obstacle(self, x, y):
		if not self.world.in_bounds(x, y):
			return True

		if self.kb.ask(Predicate('Obstacle', (x, y))):
			return True
		elif self.kb.ask(Predicate('Obstacle', (x, y), negated=True)):
			return False

		return None

	def is_explored(self, x, y):
		if not self.world.in_bounds(x, y):
			return True

		if self.kb.ask(Predicate('Explored', (x, y))):
			return True
		elif self.kb.ask(Predicate('Explored', (x, y), negated=True)):
			return False

	def print_map(self):
		safe = {}
		wumpus = {}
		pit = {}
		obs = {}
		explored = {}
		for y, x in product(reversed(range(self.world.size)), range(self.world.size)):
			safe[(x,y)] = self.is_safe(x, y)
			wumpus[(x,y)] = self.is_wumpus(x, y)
			pit[(x,y)] = self.is_pit(x, y)
			obs[(x,y)] = self.is_obstacle(x, y)
			explored[(x,y)] = self.is_explored(x, y)
		for y, x in product(reversed(range(self.world.size)), range(self.world.size)):
			if (x, y) == (self.x, self.y):
				if self.dir == NORTH:
					print('^ ', end='')
				elif self.dir == EAST:
					print('> ', end='')
				elif self.dir == SOUTH:
					print('v ', end='')
				if self.dir == WEST:
					print('< ', end='')
			elif explored[(x,y)] == True:
				print('* ', end='')
			elif obs[(x,y)] == True:
				print('X ', end='')
			elif safe[(x,y)] == True:
				print('- ', end='')
			elif wumpus[(x,y)] == True:
				print('W ', end='')
			elif pit[(x,y)] == True:
				print('P ', end='')
			else:
				print('? ', end='')

			if x == self.world.size-1:
				print()

	def take_action(self, action):
		if action == MOVE:
			if not self.world.move_explorer():
				if self.dir == NORTH:
					self.y += 1
				elif self.dir == EAST:
					self.x += 1
				elif self.dir == SOUTH:
					self.y -= 1
				elif self.dir == WEST:
					self.x -= 1
		elif action == TURN_LEFT:
			self.world.turn_explorer_left()
			self.dir -= 1
			if self.dir == 0: self.dir = WEST
		elif action == TURN_RIGHT:
			self.world.turn_explorer_right()
			self.dir = (self.dir % 4) + 1
		elif action == SHOOT: # TODO
			try: self.world.shoot_arrow()
			except: pass
		elif action == GRAB:
			self.world.pick_up_gold()
		else:
			raise ValueError('invalid action')


def get_adj(x, y, d):
	if d == NORTH:
		return (x, y+1)
	elif d == EAST:
		return (x+1, y)
	elif d == SOUTH:
		return (x, y-1)
	elif d == WEST:
		return (x-1, y)

### named constants

NORTH = 1
EAST  = 2
SOUTH = 3
WEST  = 4


MOVE = 10
TURN_LEFT = 11
TURN_RIGHT = 12
SHOOT = 13
GRAB = 14

if __name__ == '__main__':
	world = generate_world(5, 0.1, 0.1, 0.1)
	agent = ManualAgent(world)
	try:
		agent.run()
	except DeathException as de:
		print(de)
		print(world.explorer.location)
	except RecursionError:
		print('RecursionError')
	print(world.explorer.payoff)
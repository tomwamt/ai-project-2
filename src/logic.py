try:
	import colorama
	colorama.init()
	GREEN = colorama.Fore.GREEN
	RED = colorama.Fore.RED
	YELLOW = colorama.Fore.YELLOW
	BLUE = colorama.Fore.CYAN
	RESET = colorama.Fore.RESET
except ImportError:
	GREEN = ''
	RED = ''
	YELLOW = ''
	BLUE = ''
	RESET = ''

from itertools import product

OUTPUT = False # debug output
funcs = {} # evaluatable functions as string/pyfunction pairs
SEARCH_LIMIT = 50 # stop searching after this many resolutions
		
# A knowledge base and inference engine
class KnowledgeBase():
	def __init__(self):
		self.clauses = set() # no duplicates in a set
		self.cutPredicates = set()

	# simple add it if it's not already there
	def tell(self, assertion):
		if isinstance(assertion, Predicate):
			assertion = Clause(assertion)
		self.clauses.add(assertion)

	# Query KB for a result
	def ask(self, query):
		# query must be a predicate
		if not isinstance(query, Predicate):
			raise ValueError()

		if OUTPUT:
			print(BLUE+'Begin query: '+str(query)+RESET)
			input()

		# If the query is already subsumed by KB, true
		if self.subsumes(query):
			if OUTPUT: print(GREEN+'Query subsumed by knowledge base.'+RESET)
			return True
		# If negation subsumed by KB, false
		elif self.subsumes(query.clone_negated()):
			if OUTPUT: print(RED+'Negated query subsumed by knowledge base.'+RESET)
			return False
		# otherwise time to infer
		else:
			newKb = self.clone() # make a copy to use for inference
			sos = set() # set of support
			neg = Clause(query.clone_negated()) # negate the query
			newKb.tell(neg) # and tell it
			sos.add(neg) # of course query starts the SoS
			
			result = newKb.find_contradiction(sos) # find a contradiction!
			if result: # if one was found
				self.tell(query) # add the original query in
				if OUTPUT:
					print(GREEN+'Telling '+str(query)+RESET)
					input()
			return result

	# makes a copy of the KB
	def clone(self):
		new = KnowledgeBase()
		new.clauses = set(self.clauses)
		new.cutPredicates = self.cutPredicates
		return new

	# searches for a contrafiction within the KB
	def find_contradiction(self, sos):
		original = list(sos)[0]
		for i in range(SEARCH_LIMIT):
			# Sort the clause pairs by length (unit preference)
			sortedPairs = sorted(((c1, c2) for c1 in self.clauses for c2 in sos if c1 != c2), key=lambda e: len(e[0]) + len(e[1]))
			for c1, c2 in sortedPairs:
				newClause = resolution(c1, c2) # perform resolution to create a new clause
				if newClause is not None: # if it succeeeded
					if any(any(cut.subsumes(p) for cut in self.cutPredicates) for p in newClause):
						if OUTPUT: print(RED+'{} was cut.'.format(newClause)+RESET)
						continue
					if len(newClause) == 0:
						if OUTPUT: print(GREEN+'Contradiction reached!'+RESET)
						if OUTPUT: newClause.printTree(0) # print the proof tree
						return True
					elif not self.subsumes(newClause): # constradiction not found, try again
						if OUTPUT: print(BLUE+'Telling '+str(newClause)+RESET)
						self.tell(newClause)
						sos.add(newClause)
						break # continue main loop
			else: # no new clauses could be resolved
				if OUTPUT:
					print(RED+'All clauses searched: '+RESET+'Could not disprove '+str(original))
					input()
				return False
		if OUTPUT: 
			print(RED+'Search limit reached, assuming no contradiction'+RESET)
			input()
		return False
			
	# test if a clause is subsumed by the KB	
	def subsumes(self, other):
		if isinstance(other, Predicate):
			other = Clause(other)
		return any(clause.subsumes(other) for clause in self.clauses)

# generate a unique variable name (standardizing apart)
vnum = 0;
def nextVarName():
	global vnum
	vnum += 1
	return hex(vnum)[1:]

# fill vs with all variable names
# vs is a set, so unique var names only
def find_vars(comp, vs):
	for a in comp.args:
		if isinstance(a, str):
			vs.add(a)
		elif isinstance(a, Function):
			find_vars(a, vs)

# Defines a clause
class Clause():
	def __init__(self, *terms, standardize=True):
		terms = set(terms) # Eliminate duplicates, then convert back to tuple
		self.parents = None

		if standardize:
			# standardize apart
			vs = set()
			for p in terms:
				find_vars(p, vs)

			subst = []
			for v in vs:
				subst.append((v, nextVarName()))

			self.terms = tuple(apply_subst(pred, subst) for pred in terms)
		else:
			self.terms = tuple(terms)

	# equality check
	def __eq__(self, other):
		return (len(self.terms) == 1 and isinstance(other, Predicate) and self.terms[0] == other) or (isinstance(other, Clause) and self.terms == other.terms)

	def __hash__(self):
		if len(self.terms) == 1:
			return hash(self.terms[0])
		else:
			return 13 + 7 * hash(self.terms)

	def __contains__(self, item):
		return item in self.terms

	def __iter__(self):
		return iter(self.terms)

	def __len__(self):
		return len(self.terms)

	def __str__(self):
		return '[{}]'.format(' | '.join(str(t) for t in self.terms))

	# returns true if the clause contains two of the same predicate, only differing in negation
	def is_trivial(self):
		for p1, p2 in product(self.terms, self.terms):
			if p1.equals_ignore_neg(p2) and p1.negated != p2.negated:
				return True
		return False

	def subsumes(self, other):
		return len(self) == len(other) and all(any(selfPred.subsumes(otherPred) for otherPred in other) for selfPred in self) and all(any(selfPred.subsumes(otherPred) for selfPred in self) for otherPred in other)

	def printTree(self, depth):
		if self.parents is None:
			print('    '*depth + str(self))
		else:
			self.parents[0].printTree(depth+1)
			print('    '*depth + str(self))
			self.parents[1].printTree(depth+1)

# Empty class to name both Predicates and Functions as things with names and args
class Compound():
	pass

# A predicate with a name, arguments, and a negation status
# Yes I realize that negation=True is an oxymoron
class Predicate(Compound):
	def __init__(self, name, args, negated=False):
		self.name = name
		self.args = args
		self.negated = negated

	def __eq__(self, other):
		return isinstance(other, Predicate) and self.name == other.name and self.args == other.args and self.negated == other.negated

	def __hash__(self):
		h = 13
		h = h * 17 + hash(self.name)
		h = h * 23 + hash(self.args)
		h = h * 5 + hash(self.negated)
		return h

	def __str__(self):
		s = ''
		if self.negated:
			s = '~'
		s += self.name + '(' + ', '.join(str(a) for a in self.args) + ')'
		return s

	def __repr__(self):
		return 'Predicate({!r}, {!r}, negated={!r})'.format(self.name, self.args, self.negated)

	# create a copy, but negated
	def clone_negated(self):
		return Predicate(self.name, self.args, not self.negated)

	def equals_ignore_neg(self, other):
		return isinstance(other, Predicate) and self.name == other.name and self.args == other.args

	def subsumes(self, other):
		return self.name == other.name and self.negated == other.negated and all(arg_subsumes(self.args[i], other.args[i]) for i in range(len(self.args)))

# A function with a name and arguments
class Function(Compound):
	def __init__(self, name, args):
		self.name = name
		self.args = args

	def __eq__(self, other):
		return isinstance(other, Function) and self.name == other.name and self.args == other.args

	def __hash__(self):
		h = 7
		h = h * 23 + hash(self.name)
		h = h * 13 + hash(self.args)
		return h

	def __str__(self):
		return '{}({})'.format(self.name, ', '.join(str(a) for a in self.args))

	def __repr__(self):
		return 'Function({!r}, {!r})'.format(self.name, self.args)

	# is it possible to evaluate this function?
	def can_evaluate(self):
		return self.name in funcs and all(isinstance(x, int) for x in self.args)

	# Evaluate this function and return the result
	def evaluate(self):
		f = funcs[self.name]
		return f(*self.args)

	def subsumes(self, other):
		return self.name == other.name and all(arg_subsumes(self.args[i], other.args[i]) for i in range(len(self.args)))

# straight from the book
# subst is None means failure
def unify(x, y, subst):
	#if OUTPUT: input('Calling unify: x={} y={} subst={}'.format(x, y, subst))
	if subst is None:
		return None
	elif x == y:
		return subst
	elif isinstance(x, str): # variable?(x)
		return unify_var(x, y, subst)
	elif isinstance(y, str): # variable?(y)
		return unify_var(y, x, subst)
	elif isinstance(x, Function) and x.can_evaluate(): # If x is an evaluatable function, try to unify it with y even if y is not a function
		return unify(x.evaluate(), y, subst)
	elif isinstance(y, Function) and y.can_evaluate(): # If y is an evaluatable function, try to unify it with x even if x is not a function
		return unify(x, y.evaluate(), subst)
	elif isinstance(x, Compound) and isinstance(y, Compound): # coumpund?(x) and compound?(y)
		if x.name != y.name:
			return None
		else:
			return unify(x.args, y.args, subst)
	elif isinstance(x, tuple) and isinstance(y, tuple): # argument list for both x and y
		return unify(x[1:], y[1:], unify(x[0], y[0], subst))
	else:
		return None

# straight out of the book
def unify_var(var, x, subst):
	for s in subst:
		if s[0] == var:
			return unify(s[1], x, subst)
		elif s[0] == x:
			return unify(var, s[1], subst)
	if occur_check(var, apply_subst(x, subst)):
		return None
	else:
		subst.append((var, x))
		return subst

# homebrew occurs check, seems to work
def occur_check(var, x):
	if var == x:
		return True
	elif isinstance(x, str):
		return False
	elif isinstance(x, Compound):
		return any(occur_check(var, a) for a in x.args)

# apply a substitution to an object by replacing the variables with values
def apply_subst(x, subst):
	if len(subst) == 0:
		return x
	elif isinstance(x, str):
		for s in subst:
			if s[0] == x:
				return apply_subst(s[1], subst)
		return x
	elif isinstance(x, Clause):
		return Clause(*tuple(apply_subst(term, subst) for term in x), standardize=False) # I don't want to change existing variable names, so dont standardize
	elif isinstance(x, Function):
		f = Function(x.name, apply_subst(x.args, subst))
		if f.can_evaluate():
			f = f.evaluate()
		return f
	elif isinstance(x, Predicate):
		return Predicate(x.name, apply_subst(x.args, subst), x.negated)
	elif isinstance(x, tuple) and len(x) > 0:
		return (apply_subst(x[0], subst),) + apply_subst(x[1:], subst)
	else:
		return x

def arg_subsumes(a, b):
	if isinstance(a, str):
		return True
	elif isinstance(a, Function) and isinstance(b, Function):
		return a.subsumes(b)
	elif isinstance(a, int) and isinstance(b, int):
		return a == b
	else:
		return False

# perform resolution on two clauses to return a new clause, if possible
def resolution(c1, c2):
	for p1, p2 in product(c1, c2):
		if p1.name == p2.name and p1.negated != p2.negated:
			subst = unify(p1, p2, [])
			if subst is not None:
				if OUTPUT: print('Unified {} and {} with {}'.format(p1, p2, subst))
				up = apply_subst(p1, subst)
				uc1 = apply_subst(c1, subst)
				uc2 = apply_subst(c2, subst)
				if OUTPUT: print('\tUnified clauses: '+str(uc1)+' --- '+str(uc2))
				

				tc1 = list(uc1)
				tc1.remove(up)
				tc2 = list(uc2)
				tc2.remove(up.clone_negated())
				newTerms = tc1 + tc2

				newClause = Clause(*newTerms)
				newClause.parents = (c1, c2)
				newClause.subst = subst

				if not newClause.is_trivial():
					if OUTPUT: print('Resolving: {} + {} = {}{}{}'.format(uc1, uc2, YELLOW, newClause, RESET))
					return newClause
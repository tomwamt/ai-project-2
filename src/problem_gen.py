from random import random, choice as randchoice

EMPTY = ' '
PIT = '_'
OBSTACLE = 'X'
WUMPUS = 'W'
GOLD = '.'

# Thrown when the explorer dies
class DeathException(Exception):
	def __init__(self, fate, location):
		self.fate = fate
		self.location = location
		if fate == 'WUMPUS':
			message = 'The explorer was eaten by a Wumpus. Mmm, tasty.'
		elif fate == 'PIT':
			message = 'The explorer fell into a bottomless pit. Sometimes you can still hear him scream.'
		elif fate == 'SUICIDE':
			message = 'The explorer stabbed himself in the stomach with an arrow. He\'d rather die on his own terms.'
		super(DeathException, self).__init__(message)


# Holds all information regarding the explorer
class Explorer():
	def __init__(self, numArrows, location):
		self.location = location
		self.direction = randchoice([(1, 0), (-1, 0), (0, 1), (0, -1)])
		self.payoff = 0
		self.arrows = numArrows
		self.deaths = {
			'wumpus': 0,
			'pit': 0,
		}
		self.kills = 0

# Holds all information regarding the world
class WumpusWorld():
	def __init__(self, grid, numWumpi, playerLoc):
		self.size = len(grid)
		self._grid = grid
		self.explorer = Explorer(numWumpi, playerLoc)
		self.finished = False
		self.won = False
		self.scream = False
		self.bump = False

	def in_bounds(self, x, y):
		return (x < self.size and x >= 0) and (y < self.size and y >= 0)

	def _get_cell(self, x, y):
		return self._grid[y][x]

	def _set_cell(self, x, y, newVal):
		self._grid[y][x] = newVal

	# Returns the current percept vector
	def get_percepts(self):
		smell = False
		wind = False
		scream = self.scream
		bump = self.bump

		self.scream = False
		self.bump = False

		x, y = self.explorer.location

		if x+1 < self.size:
			smell = smell or self._get_cell(x+1, y) == WUMPUS
			wind = wind or self._get_cell(x+1, y) == PIT
		if x-1 >= 0:
			smell = smell or self._get_cell(x-1, y) == WUMPUS
			wind = wind or self._get_cell(x-1, y) == PIT

		if y+1 < self.size:
			smell = smell or self._get_cell(x, y+1) == WUMPUS
			wind = wind or self._get_cell(x, y+1) == PIT
		if y-1 >= 0:
			smell = smell or self._get_cell(x, y-1) == WUMPUS
			wind = wind or self._get_cell(x, y-1) == PIT

		glitter = self._get_cell(x, y) == GOLD

		return smell, wind, glitter, scream, bump

	# moves the explorer forward one square
	def move_explorer(self):
		newLoc = (self.explorer.location[0] + self.explorer.direction[0], self.explorer.location[1] + self.explorer.direction[1])
		self.explorer.payoff -= 1

		if not self.in_bounds(*newLoc) or self._get_cell(*newLoc) == OBSTACLE:
			self.bump = True
			return True

		if self._get_cell(*newLoc) == PIT:
			self.explorer.payoff -= 1000
			self.explorer.deaths['pit'] += 1
			raise DeathException('PIT', newLoc)

		if self._get_cell(*newLoc) == WUMPUS:
			self.explorer.payoff -= 1000
			self.explorer.deaths['wumpus'] += 1
			raise DeathException('WUMPUS', newLoc)

		self.explorer.location = newLoc
		return False

	# turns the explorer counter-clockwise
	def turn_explorer_left(self):
		self.explorer.direction = (-self.explorer.direction[1], self.explorer.direction[0])
		self.explorer.payoff -= 1

	# turns the explorer clockwise
	def turn_explorer_right(self):
		self.explorer.direction = (self.explorer.direction[1], -self.explorer.direction[0])
		self.explorer.payoff -= 1

	# The explorer shoots an arrow
	def shoot_arrow(self):
		if self.explorer.arrows == 0:
			raise RuntimeError('Out of arrows')

		self.explorer.arrows -= 1
		self.explorer.payoff -= 10
		arrowLoc = self.explorer.location
		direction = self.explorer.direction
		while self.in_bounds(*arrowLoc):
			if self._get_cell(*arrowLoc) == WUMPUS:
				self._set_cell(*arrowLoc, EMPTY)
				self.explorer.payoff += 10
				self.explorer.kills += 1
				self.scream = True
				return

			if self._get_cell(*arrowLoc) == OBSTACLE:
				return

			arrowLoc = (arrowLoc[0] + direction[0], arrowLoc[1] + direction[1])

	# Picks up the gold, if it's there
	def pick_up_gold(self):
		if self._get_cell(*self.explorer.location) == GOLD:
			self.explorer.payoff += 1000
			self.won = True
			self.finished = True

	# terminates the game without winning
	def give_up(self):
		self.finished = True
		raise DeathException('SUICIDE', None)

	# creates an identical copy of the world.
	# to be used right after the world is created
	def clone(self):
		newGrid = list(list(row) for row in self._grid)
		new = WumpusWorld(newGrid, 0, (0,0))
		new.explorer.location = self.explorer.location
		new.explorer.direction = self.explorer.direction
		new.explorer.payoff = self.explorer.payoff
		new.explorer.arrows = self.explorer.arrows
		return new

	# prints the world as a grid of characters
	def print_grid(self):
		for y in reversed(range(self.size)):
			for x in range(self.size):
				if (x, y) == self.explorer.location:
					if self.explorer.direction == (0, 1):
						print('^ ', end='')
					elif self.explorer.direction == (1, 0):
						print('> ', end='')
					elif self.explorer.direction == (0, -1):
						print('v ', end='')
					elif self.explorer.direction == (-1, 0):
						print('< ', end='')
				elif self._get_cell(x, y) == OBSTACLE:
					print('X ', end='')
				elif self._get_cell(x, y) == WUMPUS:
					print('W ', end='')
				elif self._get_cell(x, y) == PIT:
					print('P ', end='')
				elif self._get_cell(x, y) == GOLD:
					print('@ ', end='')
				else:
					print('- ', end='')
			print()

# factory to create a new Wumpus World with the given probabilities
def generate_world(size, pitP, obsP, wumP):
	if pitP + obsP + wumP > 1:
		raise ValueError('Invalid probability distribution')

	pitR = (0, pitP)
	obsR = (pitR[1], pitR[1] + obsP)
	wumR = (obsR[1], obsR[1] + wumP)

	empties = []
	wumpi = 0
	def random_cell(x, y):
		nonlocal wumpi
		r = random()
		if (r - pitR[0]) * (r - pitR[1]) < 0:
			return PIT
		elif (r - obsR[0]) * (r - obsR[1]) < 0:
			return OBSTACLE
		elif (r - wumR[0]) * (r - wumR[1]) < 0:
			wumpi += 1
			return WUMPUS
		else:
			empties.append((x, y))
			return EMPTY

	grid = [[random_cell(j, i) for i in range(size)] for j in range(size)]
	if len(empties) == 0:
		grid[0][0] = EMPTY
		empties.append((0, 0))
		
	player = randchoice(empties)

	gx, gy = randchoice(empties)
	grid[gy][gx] = GOLD

	world = WumpusWorld(grid, wumpi, player)

	return world

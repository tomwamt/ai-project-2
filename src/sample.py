import logic
logic.OUTPUT = True
from logic import *

if __name__ == '__main__':
	kb = KnowledgeBase()

	Jack = 1
	Tuna = 2
	Curiosity = 3

	kb.tell(Clause(
		Predicate('Animal', (Function('F', ('x_a1',)),)),
		Predicate('Loves', (
			Function('G', ('x_a1',)),
			'x_a1'
		))
	))
	kb.tell(Clause(
		Predicate('Loves', (
			'x_a2',
			Function('F', ('x_a2',))
		), negated=True),
		Predicate('Loves', (
			Function('G', ('x_a2',)),
			'x_a2'
		))
	))
	kb.tell(Clause(
		Predicate('Loves', ('y_b', 'x_b'), negated=True),
		Predicate('Animal', ('z_b',), negated=True),
		Predicate('Kills', ('x_b', 'z_b'), negated=True)
	))
	kb.tell(Clause(
		Predicate('Animal', ('x_c',), negated=True),
		Predicate('Loves', (Jack, 'x_c'))
	))
	kb.tell(Clause(
		Predicate('Kills', (Jack, Tuna)),
		Predicate('Kills', (Curiosity, Tuna))
	))
	kb.tell(Clause(
		Predicate('Cat', (Tuna,))
	))
	kb.tell(Clause(
		Predicate('Cat', ('x_f',), negated=True),
		Predicate('Animal', ('x_f',))
	))

	result = kb.ask(Predicate('Kills', (Curiosity, Tuna)))
	print('Curiosity killed the cat: {}'.format(result))
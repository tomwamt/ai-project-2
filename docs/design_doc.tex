\documentclass{article}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subcaption}
\usepackage{graphicx}
\graphicspath{{img/}}
\usepackage{algorithm}
\usepackage{algpseudocode}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}

\title{Design Document \\ \large Project \#2}
\author{Group 20:\\Jason Sanders, Travis Stone, Tom Warner}
\date{\today}

\begin{document}
\maketitle

\section{Problem Statement}
\paragraph{}
The Wumpus World is a variable-size, two-dimensional map that contains a variety of obstacles and dangers that are placed randomly upon generation. The dangers - Wumpi (the plural of Wumpus) and pitfalls - mean instant death for the player. If they occupy the same coordinates on the map as one of these dangers, they die. To avoid this, being in a square directly adjacent to one of these dangers will provide the player with a percept. Pits generate a breeze, while Wumpi generate a smell.

\paragraph{}
Aside from dangers, obstacles are included in the world that box the player into the bounds of the map as well as bar them from entering certain squares. They will not receive any sensory percept from these boundaries until they walk directly into them, where they will feel themselves run into it.

\paragraph{}
The player has one tool at their disposal to help them deal with obstacles - a bow with just as many arrows as there are Wumpi. At any time, the player will be allowed to shoot an arrow in the direction they are facing. If the arrow hits a Wumpus, the Wumpus will die and the player will receive a scream percept. If the arrow hits anything else in the straight line it is shot down, the player receives nothing.

\paragraph{}
The end goal of the Wumpus World is to navigate to the gold, which is an object placed in a random square not otherwise occupied by a pit or a Wumpus at the generation of the map. When the player is standing on the same square as the gold, they will see it glimmering in the dark. They can then grab it and return to the exit.

\paragraph{}
Further, we will be attempting to maximize the player's score. The gold is worth 1000 points. Killing a Wumpus is worth 10 points. Firing an arrow costs 10 points. Moving one square forward and turning ninety degrees both cost one point. If the player dies, they lose 1000 points.

\paragraph{}
The problem our group is trying to solve in relation to the Wumpus World is the development of an Agent that utilizes First Order Logic to explore the map safely, obtain the gold, and do so while maximizing its score. 

\section{Software Architecture}
\paragraph{}
We will be implementing our solution for the Wumpus World problem in Python 3. Our solution will consist of five classes: WumpusWorld, WumpusAgent, KnowledgeBase, Clause, and Predicate, related as shown in the UML class diagram in figure \ref{UML}.

\begin{figure}
\begin{center}
\includegraphics[width=0.6\textwidth]{UML}
\end{center}
\caption{Wumpus World software architecture}
\label{UML}
\end{figure}

\paragraph{}
\textsc{WumpusWorld} is the class the holds all the environmental data about the WumpusWorld problem. This class exposes methods to the agent for it to receive percepts and perform actions. The \texttt{get\_percepts()} method returns a percept vector for the current state of the Wumpus World. The \texttt{move\_explorer()}, \texttt{turn\_explorer\_left()}, \texttt{turn\_explorer\_right()}, \texttt{shoot\_arrow()}, and \texttt{pick\_up\_gold()} methods perform the corresponding actions in the world. Additionally, if the agent decides the current world is unsolvable, it may call \texttt{give\_up()} to terminate the execution of the solution.

\paragraph{}
\textsc{WumpusAgent} is the class that represents the logical agent. It will contain a reference to a \textsc{WumpusWorld} object that is the environment the agent interacts with. As it is a logic-based agent, it will also have an instance of \textsc{KnowledgeBase}. It will have a single method \texttt{run()} that will start the main agent loop.

\paragraph{}
\textsc{KnowledgeBase} is the class that will implement our knowledge base and inference engine. This class will implement the following 3 methods. \texttt{tell()} inserts a fact or rule into the knowledge base. \texttt{ask()} queries the knowledge base and asks it to prove the given statement, returning True if successful and False otherwise. This method performs resolution in an attempt to prove the statement. \texttt{ask\_vars()} queries the knowledge base to unify the given statement with the facts in the knowledge base, returning all substitutions that make the statement true.

\paragraph{}
\textsc{Predicate} is a simple class that represents a first order logical statement. It consists of the name of the predicate, the values assigned to each of the variables, and a flag of whether the predicate is negated or not. It supports equality checking; comparing two \textsc{Predicate} objects with the same name, the same values, and the same negation flag will return that they are equal.

\paragraph{}
Finally, the \textsc{Clause} class is simply an unordered set of predicates, any number of which may be negated. It supports equality checking (checking if two clauses contain the exact same predicate objects), checking if a predicate object is contained within the clause, and iterating through the predicates in the clause.

\paragraph{}
There are also several helper functions to be used in the inference process, including \texttt{unify()} to unify two predicates, \texttt{apply\_substitution()} to apply a substitution string to a predicate, and \texttt{resolve()} to perform resolution on two clauses.

\section{Design Decisions}
\paragraph{}
Our knowledge base facts and rules will consist entirely of clauses. Free variables in these clauses will be implicitly qualified by universal qualifiers. We will write our rules to never use existential quantifiers. This will avoid the use of Skolem functions and constants, at the cost of longer clauses.

\paragraph{}
Values in predicate statements will be constants or variables (again, implicitly qualified by universals). Known constants will be stored internally as non-negative integers, categorized into several domains. Universally quantified variables will be denoted using strings. These strings will be uniquely generated to avoid the need for standardizing apart.

\paragraph{}
Our agent will decide which action to take based on the use of a predicate called PossibleAction(time, action, danger). The agent will query the knowledge base for this predicate, asking it to give danger values for various actions. The agent will then pick the action with the lowest danger, breaking ties randomly.

\paragraph{}
Our knowledge base will use simple linear search and backtracking to find clauses during resolution. While this is a slow and inefficient method, it is also easy to implement and debug, which will help with program correctness

\section{Experimental Design}
\paragraph{}
To test our agent, we will inject it into Wumpus Worlds of various sizes ranging from 5-by-5 to 25-by-25 and with different placements of obstacles, dangers, and gold. The information we will be drawing from each test will be how many times the agent moved, how many arrows it fired, how many times it hit a Wumpus with an arrow, whether or not it grabbed the gold, and whether it died or gave up.

\paragraph{}
We will run the agent on at least 20 runs of each world size, from 5 to 25. We use at least 10 different configurations of varying the problem generation parameters for each world size, and run the agent at least twice per configuration. As time permits, we will run more test per configuration or more configurations to gather more data.

\paragraph{}
Our measure of success will be whether or not the agent can reliably grab the gold and solve the map, and we will measure how score and success rate vary with world generation parameters.

\section{References}
[1] S.  Russell, P.  Norvig and E.  Davis, \textit{Artificial Intelligence}. Upper Saddle River, NJ: Prentice Hall, 2010.\\[0pt]

\end{document}
Predicates:
	Action(t, a) - At time t, the explorer took action a
	#Adjacent(a, b, x, y, d) - Cell (a, b) is in d direction of cell (x, y)
	#Bump(t) - Cell in desired direction from a,b is an obstacle. 
	#ClearShotH(t, x1, x2, y) - At time t, there is a clear shot from cell (x1, y) to (x2, y)
	#ClearShotV(t, x, y1, y2) - At time t, there is a clear shot from cell (x, y1) to (x, y2)
	#Direction(t, d) - At time t, explorer faces in direction d
	#Equals(a,b) - A and B are equal things
	#Explored(t, x, y) - At time t, cell (x, y) has been explored
	#Gold(x, y) - There is gold in cell (x, y)
	#Greater(x, y) - x is greater than y
	#Less(x, y) - x is less than y
	#Location(t, x, y) - At time t, explorer is in cell (x, y)
	#NumArrows(t, n) - At time t, explorer has n arrows
	#Obstacle(x, y) - There is an obstacle in cell (x, y)
	#Pit(x, y) - There is a pit in cell (x, y)
	#Safe(t, x, y) - At time t, cell (x, y) is safe
	#Smell(t, x, y) - At time t, a there is a smell in cell (x, y)
	#Wind(x, y) - There is wind in cell (x, y)
	#Wumpus(t, x, y) - At time t, there is a wumpus in cell (x, y)

Clauses:
	!Smell(a,b) and !Wind(a,b) -> ForAll(d)(Adjacent(x,y,a,b,d) -> Safe(t,x,y))

	!Wumpus(t,a,b) and !Pit(a,b) -> Safe(t,a,b)

	Smell(a,b) -> Exists(d)(Adjacent(x,y,a,b,d) -> Wumpus(t,x,y))

	Wind(a,b) -> Exists(d)(Adjacent(x,y,a,b,d) and Pit(x,y))

	Gold(a,b) and Location(t,a,b) -> Action(t, GRAB)

	Location(t,a,b) and Action(t, MOVE) and Adjacent(c,e,a,b,d) and Obstacle(c,e) -> Location(t+1, a, b)

	Location(t,a,b) and Bump(t) -> Obstacle(x,y) and Adjacent(x,y,a,b,d) and Location(t+1,a,b)

	Wumpus(t,a,b) -> ForAll(d)(Adjacent(x,y,a,b,d) -> Smell(t,x,y))

	Pit(a,b) -> ForAll(d)(Adjacent(x,y,a,b,d) -> Wind(x,y)

	Explored(t,x,y) -> Safe(t,x,y)

	Equal(a,a)

	Equal(a,b) and Equal(b,c) -> Equal(a,c)

	Equal(a,b) -> Equal(b,a)

	!Explored(t,a,b) and Location(t+1,a,b) and Greater(t+1,t) -> Explored(t,a,b)

	!Equal(abs(x), x) -> Less(x,0)

	!Less(a,b) and !Equal(a,b) -> Greater(a,b)

	ForAll(a,b)(Less(sub(a,b), 0)) -> Less(a,b)

	Location(t,a,b) and Action(t,MOVE) and Direction(t,d) -> (Adjacent(x,y,a,b,d) -> Location(t+1,x,y) and Direction(t+1,d))

	Location(t,a,b) -> Explored(t,a,b)

	NumArrows(t, n) and Action(t, fire) -> NumArrows(t+1, n-1)

	NumArrows(t, n) and !Action(t, fire) -> NumArrows(t+1, n)

	Location(t,a,b) and Direction(t, d) and Action(t, move) -> ForAll(x,y):(Adjacent(x,y,a,b,d)) -> Location(t+1,x,y)

	Direction(t, NORTH) and Action(t, Turn_Left) -> Direction(t+1, WEST)

	Direction(t, WEST) and Action(t, Turn_Left) -> Direction(t+1, SOUTH)

	Direction(t, SOUTH) and Action(t, Turn_Left) -> Direction(t+1, EAST)

	Direction(t, EAST) and Action(t, Turn_Left) -> Direction(t+1, NORTH)

	Direction(t, NORTH) and Action(t, Turn_Right) -> Direction(t+1, EAST)

	Direction(t, EAST) and Action(t, Turn_Right) -> Direction(t+1, SOUTH)

	Direction(t, SOUTH) and Action(t, Turn_Right) -> Direction(t+1, WEST)

	Direction(t, WEST) and Action(t, Turn_Right) -> Direction(t+1, NORTH)

	Adjacent(a,b,x,y,EAST) <-> Adjacent(x,y,a,b,WEST)

	Adjacent(a,b,x,y,NORTH) <-> Adjacent(x,y,a,b,SOUTH)

	ForAll(x,y):Adjacent(x,y,a,b,NORTH) <-> Equal(b,y+1) and Equal(a,x)

	ForAll(x,y):Adjacent(x,y,a,b,SOUTH) <-> Equal(b,y-1) and Equal(a,x)

	ForAll(x,y):Adjacent(x,y,a,b,EAST) <-> Equal(a,x+1) and Equal(b,y)

	ForAll(x,y):Adjacent(x,y,a,b,WEST) <-> Equal(a,x-1) and Equal(b,y)

	Safe(t,a,b) -> Safe(t+1,a,b)

	Explored(t,a,b) -> Explored(t+1,a,b)

	ClearshotH(t,x,x,y)

	ClearshotV(t,x,y,y)

	!Obstacle(x1,y) and Adjacent(x2,y,x1,y,d) and ClearshotH(t,x2,x3,y) -> ClearShot(t,x1,x3,y)

	!Obstacle(x,y1) and Adjacent(x,y2,x,y1,d) and ClearshotV(t,x,y2,y3) -> Clearshot(t,x,y1,y3)

	Scream(t) ^ Location(t,x,y) ^ Direction(t,d) ^ Adjacent(a,b,x,y,d) => ~Wumpus(t,a,b)
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subcaption}
\usepackage{graphicx}
\graphicspath{{img/}}
\usepackage{algorithm}
\usepackage{algpseudocode}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}

\title{Final Report \\ \large Project \#2}
\author{Group 20:\\Jason Sanders, Travis Stone, Tom Warner}
\date{\today}

\begin{document}
\maketitle

\abstract{The Reflex Agent always failed to find the gold, unless the danger on the map was sparse and the gold happened to be in its direct path. The Logic Agent showed some improvements over the Reflex Agent, often succeeding on worlds with more sparse danger, but failing on the more difficult worlds.}

\section{Problem Statement}
\paragraph{}
The Wumpus World is a finite, variable-size, two-dimensional grid-based map that contains a variety of obstacles and dangers placed randomly upon its generation. The world is to be navigated by an explorer. The dangers - Wumpi and pitfalls - mean instant death for the explorer. If they occupy the same coordinates on the map as one of these dangers, they will die. However, death does not terminate the world. Instead, the explorer is placed back where he was, this time with knowledge of the danger ahead. The goal of the Wumpus World problem is for the explorer to navigate around dangers and pick up a pile of gold placed somewhere within the world.

\paragraph{}
The explorer has a bow at their disposal to help them deal with Wumpi. The bow has just as many arrows as there are Wumpi in the initial state of the world. At any time, the player will be allowed to shoot an arrow in the direction they are facing. If the arrow hits a Wumpus, the Wumpus will die. Otherwise, if the arrow hits an obstacle or the edge of the world, it stops.

\paragraph{}
To avoid danger and aid in navigation, the explorer has several percepts. Being in a square directly adjacent to one of these dangers will provide the player with a percept. Pits generate a \textit{wind} percept, while Wumpi generate a \textit{smell}. If the explorer attempts to move into an obstacle or outside the bounds of the world, they will receive a \textit{bump} percept. If a Wumpus is killed by an arrow, the explorer will hear it, and will receive a \textit{scream} percept. Finally, if the explorer and the gold occupy the same square, the explorer will receive a \textit{glitter} percept.

\paragraph{}
The explorer has five actions that they are allowed to take in the world. First, they can move one square in the direction they are facing. They can also turn left or right, rotating 90 degrees. The explorer may fire an arrow in the direction they are facing. This action has no effect if the explorer has no arrows. Finally, the explorer can attempt to pick up the gold. Successfully picking up the gold terminates the world.

\paragraph{}
Furthermore, there is a payoff score associated with several events in the world. Picking up the gold awards 1000 points. Killing a Wumpus is worth ten points. Firing an arrow costs ten points. Moving one square forward and turning cost one point each. If the explorer dies, they lose 1000 points.

\paragraph{}
The problem our group has attempted to solve in relation to the Wumpus World is the development of two agents to control the explorer to explore the map safely, and obtain the gold. The first agent, called the Reflex Agent, was a simple agent that mapped a percept or combination of percepts to a single action at any given time step. The second agent, called the Logic Agent, used inference in first order logic to determine the danger of specific cells, retained information about the world, and could execute multiple actions in order to avoid danger and explore the world.

\subsection{Hypothesis}
\paragraph{}
We expected the Reflex Agent to perform poorly, and that it would often fail to find the gold. If it did, it would incur a very low score to do so. We thought that its rate of failure would only increase as the map became larger. Our reasoning behind this was that given that the Reflex Agent could make no inferences about the world (and even if it could, it had no memory of past information, so it would not retain its own inferences), could only execute atomic actions, and had to be a deterministic map of percepts to actions, it could not make intelligent decisions about the world. In short, we thought it was far too simple to effectively solve the Wumpus World problem.

\paragraph{}
On the other hand, we expected the Logic Agent to perform much more effectively. We expected that the only situations in which it would fail to find the gold are those where the configuration of the map made it unsolvable. However, we could not claim that it would achieve a high or optimal score as it would likely require a great deal of extraneous exploration (and possibly death) in order to determine danger and find the gold.

\section{Algorithms \& Program Design}
\subsection{Knowledge Base}
\paragraph{}
The engine that both agents are built off of was a first order logical knowledge base and inference engine, although the Reflex agent did not use it in its full capacity.

\paragraph{}
The knowledge base consisted of a set of clauses, each of which consisted of one or more first order predicates that may or may not have been negated. The clauses in the knowledge base were such that no clause entirely subsumed another. This eliminated redundant clauses and reduced the number of clauses overall, allowing for faster search. The knowledge base supported two main operations: telling and asking. Telling inserted a clause into the knowledge base, while asking queried it to prove a predicate statement.

\paragraph{}
The knowledge base used unification and resolution of clauses to prove queries. In specific, it used a proof by contradiction method to attempt to find a contradiction when the negated query was inserted into the knowledge base. It did this by iterating through the clauses and generating new clauses using the resolution inference rule until an empty clause was reached, representing a contradiction. At that point, the query was added to the knowledge base as a new fact. However, if either no new clauses could be generated or no contradiction could be found within a limited number of resolutions, the process would fail and the original query was assumed to still be unknown.

\paragraph{}
To speed up the proof process, we implemented two search strategies. First, the resolution process had Unit Preference, always attempting to resolve the shortest clauses first. Second, a Set of Support strategy was used, where all resolutions must use at least one clause in a set of support initially containing the original negated query, as well as any new clauses resolved in the process.

\subsection{Reflex Agent}
\paragraph{}
The Reflex Agent was implemented as a single loop, where each iteration a knowledge base was generated and populated with simple rules. Because the rules had to be simple mappings of percepts to actions, they all took the form of $Percept_1() \wedge Percept_2() \wedge \ldots \rightarrow Action(A)$ where the $Percept_i$ predicate represented a percept received at the current time step and $Action(A)$ represented taking the action $A$ at the same time step. The agent first gathered percepts from the world, told them to the knowledge base, then asked it to ``prove'' various actions to determine which one it should take. After taking an action, the current knowledge percept knowledge was discarded and the loop continued.

\paragraph{}
The three rules we chose to use for the Reflex agent were: $Bump() \rightarrow Action(TURN\_LEFT)$, $Glitter() \rightarrow Action(GRAB)$, and $\neg Bump() \wedge \neg Glitter() \rightarrow Action(MOVE)$.

\paragraph{}
Because the Reflex Agent could not remember if and when it died, we were required to terminate the program if the payoff score dropped beneath a certain threshold.

\subsection{Logic Agent}
The Logic Agent was a more sophisticated method. The most important distinctions are that it could remember facts and make inferences about the world around it. As it traversed the world, the agent gathered and stored percept information in the knowledge base. It then asked a series of queries about the squares around it in order to choose an action to perform.

\paragraph{}
The logic agent was also capable of using compound actions, such as turning several times to orient itself, then moving or firing an arrow. Turning before taking another action was acceptable because turning never changed the percepts at the current square.

\paragraph{}
The strategy the Logic Agent used is as follows: first the agent looks for a safe, unexplored adjacent square, in the order of north, east, south, then west. If it finds one, it turns to it and moves forward. Otherwise, it looks for any confirmed Wumpi in the adjacent squares. If it finds one, it fires an arrow at it. Otherwise, it picks an unexplored, potentially unsafe square and moves to it. If there are no unexplored squares adjacent, it moves in a random direction. Finally, if the agent believes there is nowhere safe left to explore, it may terminate the program early.

\paragraph{}
The facts known by the logic agent do not use either a time component or situation calculus. Instead, relevant facts are retained from one time step to the next. Facts that may change over time, however, may be ``forgotten'' and may need to be re-inferred. For example, if the agent infers the location of a Wumpus at a square $(x,y)$, it adds the fact $Wumpus(x,y)$ to the knowledge base. In the next time step, if the $scream$ percept is received, it may forget $Wumpus(x,y)$ as that Wumpus may have died.

\section{Experimental Procedure}
\paragraph{}
While testing our Logical Agent against our Reflex agent, we ran experiments on 10 different maps for each size. Each run had a different probability of generating pits, Wumpi, and obstacles. Both the Logical and Reflex agents were run on the same map from the same starting position and facing the same direction.
\paragraph{}
We gathered data on their final score, a boolean value for whether or not the agent found the gold, the number of Wumpi they killed, the times a Wumpus killed them, and the times they fell into a pit. This information will be used to compare the overall average score on a map of a given size under various configurations, observe how well the Logical agent is able to find and kill Wumpi, and - to a more limited extent - see how effective the agent is at finding the gold.
\paragraph{}
Table \ref{tests} shows the percentage probabilities for pits, obstacles, and Wumpi to be generated in each world.
\begin{table}[hb]
\centering
\caption{Test configurations}
\label{tests}
\begin{tabular}{|llll|}
\hline
        & Pit Probability & Obstacle Probability & Wumpus Probability \\ \hline
Test 1  & 0\%             & 0\%                  & 0\%                \\
Test 2  & 10\%            & 10\%                 & 10\%               \\
Test 3  & 20\%            & 20\%                 & 20\%               \\
Test 4  & 30\%            & 30\%                 & 30\%               \\
Test 5  & 10\%            & 40\%                 & 40\%               \\
Test 6  & 40\%            & 10\%                 & 40\%               \\
Test 7  & 40\%            & 40\%                 & 10\%               \\
Test 8  & 10\%            & 10\%                 & 60\%               \\
Test 9  & 10\%            & 60\%                 & 10\%               \\
Test 10 & 60\%            & 10\%                 & 10\%               \\ \hline
\end{tabular}
\end{table}
\section{Experimental Results}
\paragraph{}
Tables \ref{5x5} through \ref{25x25} showcase the results for each agent in each world size for both the Logical and Simple Agents. In the 25x25 results, four tests did not finish.

\begin{table}[p]
\centering
\caption{5x5 World Tests}
\label{5x5}
\begin{tabular}{|l|l|l|l|l|l|}
\hline
5x5 Maps         & Score  & Gold Found & Wumpi Killed & Wumpus Deaths & Pit Deaths \\ \hline
Test 1 - Reflex  & 999    & True       & 0            & 0             & 0          \\ \hline
Test 2 - Reflex  & 1000   & True       & 0            & 0             & 0          \\ \hline
Test 3 - Reflex  & -10015 & False      & 0            & 0             & 10         \\ \hline
Test 4 - Reflex  & -10012 & False      & 0            & 10            & 0          \\ \hline
Test 5 - Reflex  & 1000   & True       & 0            & 0             & 0          \\ \hline
Test 6 - Reflex  & -10010 & False      & 0            & 10            & 0          \\ \hline
Test 7 - Reflex  & 1000   & True       & 0            & 0             & 0          \\ \hline
Test 8 - Reflex  & -10013 & False      & 0            & 10            & 0          \\ \hline
Test 9 - Reflex  & -10010 & False      & 0            & 10            & 0          \\ \hline
Test 10 - Reflex & -10010 & False      & 0            & 0             & 10         \\ \hline
Test 1 - Logic   & 968    & True       & 0            & 0             & 0          \\ \hline
Test 2 - Logic   & 1000   & True       & 0            & 0             & 0          \\ \hline
Test 3 - Logic   & -10001 & False      & 5            & 3             & 3          \\ \hline
Test 4 - Logic   & -1065  & False      & 0            & 0             & 1          \\ \hline
Test 5 - Logic   & 1000   & True       & 0            & 0             & 0          \\ \hline
Test 6 - Logic   & -1088  & False      & 0            & 1             & 0          \\ \hline
Test 7 - Logic   & 1000   & True       & 0            & 0             & 0          \\ \hline
Test 8 - Logic   & -10002 & False      & 4            & 2             & 0          \\ \hline
Test 9 - Logic   & -2026  & True       & 5            & 2             & 1          \\ \hline
Test 10 - Logic  & -10001 & False      & 1            & 1             & 8          \\ \hline
\end{tabular}
\end{table}

\begin{table}[p]
\centering
\caption{10x10 World Tests}
\label{10x10}
\begin{tabular}{|l|l|l|l|l|l|}
\hline
10x10 Maps       & Score  & Gold Found & Wumpi Killed & Wumpus Deaths & Pit Deaths \\ \hline
Test 1 - Reflex  & 964    & True       & 0            & 0             & 0          \\ \hline
Test 2 - Reflex  & -10010 & False      & 0            & 10            & 0          \\ \hline
Test 3 - Reflex  & -10011 & False      & 0            & 10            & 0          \\ \hline
Test 4 - Reflex  & -10013 & False      & 0            & 10            & 0          \\ \hline
Test 5 - Reflex  & -10010 & False      & 0            & 10            & 0          \\ \hline
Test 6 - Reflex  & -10010 & False      & 0            & 10            & 0          \\ \hline
Test 7 - Reflex  & -10014 & False      & 0            & 0             & 10         \\ \hline
Test 8 - Reflex  & 1000   & True       & 0            & 0             & 0          \\ \hline
Test 9 - Reflex  & -10012 & False      & 0            & 0             & 10         \\ \hline
Test 10 - Reflex & -10010 & False      & 0            & 0             & 10         \\ \hline
Test 1 - Logic   & 869    & True       & 0            & 0             & 0          \\ \hline
Test 2 - Logic   & -8008  & True       & 9            & 5             & 3          \\ \hline
Test 3 - Logic   & 993    & True       & 0            & 0             & 0          \\ \hline
Test 4 - Logic   & -1288  & False      & 1            & 1             & 0          \\ \hline
Test 5 - Logic   & -10000 & False      & 7            & 3             & 0          \\ \hline
Test 6 - Logic   & -10026 & False      & 6            & 4             & 6          \\ \hline
Test 7 - Logic   & -1009  & False      & 0            & 0             & 1          \\ \hline
Test 8 - Logic   & 1000   & True       & 0            & 0             & 0          \\ \hline
Test 9 - Logic   & -1088  & False      & 0            & 0             & 1          \\ \hline
Test 10 - Logic  & -10001 & False      & 1            & 0             & 4          \\ \hline
\end{tabular}
\end{table}

\begin{table}[p]
\centering
\caption{15x15 Map Tests}
\label{15x15}
\begin{tabular}{|l|l|l|l|l|l|}
\hline
15x15 Maps       & Score  & Gold Found & Wumpi Killed & Wumpus Deaths & Pit Deaths \\ \hline
Test 1 - Reflex  & -10000 & False      & 0            & 0             & 0          \\ \hline
Test 2 - Reflex  & -10011 & False      & 0            & 10            & 0          \\ \hline
Test 3 - Reflex  & -10010 & False      & 0            & 0             & 10         \\ \hline
Test 4 - Reflex  & -10010 & False      & 0            & 0             & 10         \\ \hline
Test 5 - Reflex  & -10010 & False      & 0            & 10            & 0          \\ \hline
Test 6 - Reflex  & -10012 & False      & 0            & 10            & 0          \\ \hline
Test 7 - Reflex  & -10012 & False      & 0            & 0             & 10         \\ \hline
Test 8 - Reflex  & -10010 & False       & 0            & 10            & 0          \\ \hline
Test 9 - Reflex  & -10014 & False      & 0            & 0             & 10         \\ \hline
Test 10 - Reflex & -10011 & False      & 0            & 0             & 10         \\ \hline
Test 1 - Logic   & 943    & True       & 0            & 0             & 0          \\ \hline
Test 2 - Logic   & -39    & True       & 1            & 1             & 0          \\ \hline
Test 3 - Logic   & -10167 & False      & 9            & 6             & 4          \\ \hline
Test 4 - Logic   & -2009  & False      & 0            & 0             & 2          \\ \hline
Test 5 - Logic   & -3014  & False      & 1            & 1             & 2          \\ \hline
Test 6 - Logic   & -10125 & False      & 6            & 5             & 5          \\ \hline
Test 7 - Logic   & -10002 & False      & 0            & 0             & 4          \\ \hline
Test 8 - Logic   & -10062 & False      & 12           & 9             & 1          \\ \hline
Test 9 - Logic   & -1006  & False      & 0            & 0             & 1          \\ \hline
Test 10 - Logic  & -10001 & False      & 2            & 1             & 5          \\ \hline
\end{tabular}
\end{table}

\begin{table}[p]
\centering
\caption{20x20 Map Tests}
\label{20x20}
\begin{tabular}{|l|l|l|l|l|l|}
\hline
20x20 Maps       & Score  & Gold Found & Wumpi Killed & Wumpus Deaths & Pit Deaths \\ \hline
Test 1 - Reflex  & -10000 & False      & 0            & 0             & 0          \\ \hline
Test 2 - Reflex  & -10012 & False      & 0            & 10            & 0          \\ \hline
Test 3 - Reflex  & -10010 & False      & 0            & 10            & 0          \\ \hline
Test 4 - Reflex  & -10010 & False      & 0            & 0             & 10         \\ \hline
Test 5 - Reflex  & -10010 & False      & 0            & 10            & 0          \\ \hline
Test 6 - Reflex  & -10012 & False      & 0            & 10            & 0          \\ \hline
Test 7 - Reflex  & -10014 & False      & 0            & 0             & 10         \\ \hline
Test 8 - Reflex  & -10010 & False      & 0            & 10            & 0          \\ \hline
Test 9 - Reflex  & -10013 & False      & 0            & 0             & 10         \\ \hline
Test 10 - Reflex & -10010 & False      & 0            & 10            & 0          \\ \hline
Test 1 - Logic   & 690    & True       & 0            & 0             & 0          \\ \hline
Test 2 - Logic   & -10087 & False      & 5            & 6             & 4          \\ \hline
Test 3 - Logic   & -10890 & False      & 3            & 2             & 8          \\ \hline
Test 4 - Logic   & -4230  & False      & 1            & 1             & 2          \\ \hline
Test 5 - Logic   & -5788  & False      & 0            & 4             & 0          \\ \hline
Test 6 - Logic   & -10036 & False      & 7            & 5             & 5          \\ \hline
Test 7 - Logic   & -10001 & False      & 2            & 2             & 4          \\ \hline
Test 8 - Logic   & -10042 & False      & 15           & 10            & 0          \\ \hline
Test 9 - Logic   & -10000 & False      & 3            & 2             & 0          \\ \hline
Test 10 - Logic  & -6016  & False      & 1            & 1             & 5          \\ \hline
\end{tabular}
\end{table}

\begin{table}[p]
\centering
\caption{25x25 Map Tests}
\label{25x25}
\begin{tabular}{|l|l|l|l|l|l|}
\hline
25x25 Maps       & Score  & Gold Found & Wumpi Killed & Wumpus Deaths & Pit Deaths \\ \hline
Test 1 - Reflex  & 898    & True       & 0            & 0             & 0          \\ \hline
Test 2 - Reflex  & -10010 & False      & 0            & 0             & 10         \\ \hline
Test 3 - Reflex  & -10016 & False & 0 & 10 & 0  \\ \hline
Test 4 - Reflex  & -10010 & False & 0 & 0  & 10 \\ \hline
Test 5 - Reflex  & N/A    &       &   &    &    \\ \hline
Test 6 - Reflex  & -10012 & False & 0 & 0  & 10 \\ \hline
Test 7 - Reflex  & -10010 & False & 0 & 0  & 10 \\ \hline
Test 8 - Reflex  & N/A    &       &   &    &    \\ \hline
Test 9 - Reflex  & N/A    &       &   &    &    \\ \hline
Test 10 - Reflex & N/A    &       &   &    &    \\ \hline
Test 1 - Logic   & 447    & True       & 0            & 0             & 0          \\ \hline
Test 2 - Logic   & -10579 & False      & 12           & 5             & 5          \\ \hline
Test 3 - Logic   & -10512 & False & 9 & 3  & 7  \\ \hline
Test 4 - Logic   & -2007  & False & 0 & 0  & 2  \\ \hline
Test 5 - Logic   & N/A    &       &   &    &    \\ \hline
Test 6 - Logic   & -10002 & False & 6 & 2  & 4  \\ \hline
Test 7 - Logic   & -3749  & False & 0 & 1  & 2  \\ \hline
Test 8 - Logic   & N/A    &       &   &    &    \\ \hline
Test 9 - Logic   & N/A    &       &   &    &    \\ \hline
Test 10 - Logic  & N/A    &       &   &    &    \\ \hline
\end{tabular}
\end{table}

\section{Conclusions}
\paragraph{}
The most obvious conclusion that can be made from all this information is that the reflex agent and the logical agent are effectively equals when it comes to solving Wumpus Worlds with randomized placements of obstacles and hazards as well as no cap on the amount of obstacles or hazards. The median value in each table save for the five-by-five Wumpus Worlds is in the negative ten thousands. Even then, the five-by-five maps can be attributed to the fact that in several of its configurations the player starts out on top of the gold.

\paragraph{}
The matter to be considered after that is the one of how effective the agents are at finding the gold. In the 5x5 maps, the gold is found fifty percent of the time by the Reflex Agent and fifty percent of the time by the Logical Agent. With the 10x10 maps, the Reflex Agent found the gold twenty percent of the time and the Logical Agent found the gold forty percent of the time. In 15x15 maps, the Reflex Agent found the gold zero percent of the time and the Logical Agent found the gold twenty percent of the time. In 20x20 maps, the Reflex Agent found the gold zero percent of the time and the Logical Agent found the gold ten percent of the time. In 25x25 maps, the Reflex Agent found the gold X percent of the time and the Logical Agent found the gold Y percent of the time. From this, we can see that the Logical Agent is only marginally better than the Reflex Agent.

\paragraph{}
To the merit of the Logical Agent, it was able to die much less than the Reflex Agent. The Reflex Agent will never do anything but beat its head against the first hazard that appears in front of its predetermined line of trajectory and will always die ten times in a row to the same hazard. However, the Logical Agent is able to learn where these hazards are and avoid them in the future. This leads to it having fewer deaths on average in every map and it having a variety in which specific hazard leads to its death.

\paragraph{}
Some of these results may be attributed to the conditions of the worlds generated. For many of the test configurations, there was an extremely high probability of the world being unsolvable, or at the very least extremely crowded by danger. In tests 1, 2, and 3, which had lower combined probabilities of danger, the logical agent did find the gold more often than the logic agent. Additionally, in higher-danger worlds, there was a high chance of the explorer starting in the same square as the gold, making the choice of agent irrelevant.

\paragraph{}
In the end, the Logic Agent showed some improved behavior over that of the Reflex Agent, but it's overall performance was not a very large improvement.

\section{References}
[1] S.  Russell, P.  Norvig and E.  Davis, \textit{Artificial Intelligence}. Upper Saddle River, NJ: Prentice Hall, 2010.\\[0pt]


\end{document}

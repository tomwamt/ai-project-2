Descriptions:
F1: The x coordinate of a square adjacent to (x,y) is either x, x+1, or x-1
F2: The y coordinate of a square adjacent to (x,y) is either y, y+1, or y-1
F3: At least one of the coordinates of a square adjacent to (x,y) must differ from that of (x,y)
F3: At most one of the coordinates of a square adjacent to (x,y) must differ from that of (x,y)

EQ1: If you have two of the same predicate with one negated, their arguments must be different (for Wumpus predicate in specific)
EQ2: All things equal themselves (reflexive)
EQ3: If one thing equals another, the other thing equals the first (symmetric)
EQ4: If one thing equals another and the other equals a third, the first thing equals the third (transitive)

WS1: If there is a smell in a square, there must be a wumpus in at least one adjacent square
WS2: If there is a wumpus in a square, all adjacent squares must have a smell

P set: Facts that the agent has gathered so far

Functions:

def adj_x(x, y, d):
	if d == EAST:
		return x+1
	elif d == WEST:
		return x-1
	else:
		return x

def adj_y(x, y, d):
	if d == NORTH:
		return y+1
	elif d == SOUTH:
		return y-1
	else:
		return y

Rules:

F1. ((adj_x(x_f1, y_f1, d_f1) = x_f1) v (adj_x(x_f1, y_f1, d_f1) = x_f1+1) v (adj_x(x_f1, y_f1, d_f1) = x_f1-1));
F2. ((adj_y(x_f2, y_f2, d_f2) = y_f2) v (adj_y(x_f2, y_f2, d_f2) = y_f2+1) v (adj_y(x_f2, y_f2, d_f2) = y_f2-1));
F3. (~(adj_x(x_f3, y_f3, d_f3) = x_f3) v ~(adj_y(x_f3, y_f3, d_f3) = y_f3));
F4. ((adj_x(x_f4, y_f4, d_f4) = x_f4) v (adj_y(x_f4, y_f4, d_f4) = y_f4));

EQ1. (~Wumpus(t_eq1, x_eq1, y_eq1) v Wumpus(t1_eq1, x1_eq1, y1_eq1) v ~(t_eq1 = t1_eq1) v ~(x_eq1 = x1_eq1) v ~(y_eq1 = y1_eq1));
EQ2. (x_eq2 = x_eq2);
EQ3. (~(x_eq3 = y_eq3) v (y_eq3 = x_eq3));
EQ4. (~(x_eq4 = y_eq4) v ~(y_eq4 = z_eq4) v (x_eq4 = z_eq4));

WS1. (~Smell(t_ws1, x_ws1, y_ws1) v Wumpus(t_ws1, adj_x(x_ws1, y_ws1, d(t_ws1, x_ws1, y_ws1)), adj_y(x_ws1, y_ws1, d(t_ws1, x_ws1, y_ws1))));
WS2. (~Wumpus(t_ws2, x_ws2, y_ws2) v Smell(t_ws2, adj_x(x_ws2, y_ws2, d_ws2), adj_x(x_ws2, y_ws2, d_ws2)));

Facts:

P1. (Smell(5, 3, 3));
P2. (~Wumpus(5, 3, 2));
P3. (~Wumpus(5, 3, 4));
P4. (~Wumpus(5, 2, 3));



Query: (Wumpus(5, 4, 3))?

Negate theorem:
Q0. (~Wumpus(5, 4, 3));

P1 & WS1 with {t_ws1/5, x_ws1/3, y_ws1/3}
Q1. (Wumpus(5, adj_x(3, 3, d(5, 3, 3)), adj_y(3, 3, d(5, 3, 3))));

Q1 & EQ1 with {t_eq1/5, x_eq1/adj_x(3, 3, d(5, 3, 3)), y_eq1/adj_y(3, 3, d(5, 3, 3))}
Q2. (Wumpus(t1_eq1, x1_eq1, y1_eq1) v ~(5 = t1_eq1) v ~(adj_x(3, 3, d(5, 3, 3)) = x1_eq1) v ~(adj_y(3, 3, d(5, 3, 3)) = y1_eq1));

Q2 & P2 with {t1_eq1/5, x1_eq1/3, y1_eq1/2}
Q3a. (~(5 = 5) v ~(adj_x(3, 3, d(5, 3, 3)) = 3) v ~(adj_y(3, 3, d(5, 3, 3)) = 2));

Q3a & EQ2 with {x_eq2/5}
Q3b. (~(adj_x(3, 3, d(5, 3, 3)) = 3) v ~(adj_y(3, 3, d(5, 3, 3)) = 2));

Repeat Q3a/b process with P3, P4, Q0
Q4b. (~(adj_x(3, 3, d(5, 3, 3)) = 3) v ~(adj_y(3, 3, d(5, 3, 3)) = 4));
Q5b. (~(adj_x(3, 3, d(5, 3, 3)) = 2) v ~(adj_y(3, 3, d(5, 3, 3)) = 3));
Q6b. (~(adj_x(3, 3, d(5, 3, 3)) = 4) v ~(adj_y(3, 3, d(5, 3, 3)) = 3));

Q6b & F1 with {x_f1/3, y_f1/3, d_f1/d(5, 3, 3)}
Q7. (~(adj_y(3, 3, d(5, 3, 3)) = 3) v (adj_x(3, 3, d(5, 3, 3)) = 3) v (adj_x(3, 3, d(5, 3, 3)) = 2));

Q7 & Q5b with {}
Q8. (~(adj_y(3, 3, d(5, 3, 3)) = 3) v (adj_x(3, 3, d(5, 3, 3)) = 3));

Q8 & F3 with {x_f3/3, y_f3/3, d_f3/d(5, 3, 3)}
Q9. (~(adj_y(3, 3, d(5, 3, 3)) = 3));

Q9 & F2 with {x_f2/3, y_f2/3, d_f2/d(5, 3, 3)}
Q10. ((adj_y(3, 3, d(5, 3, 3)) = 4) v (adj_y(3, 3, d(5, 3, 3)) = 2));

Q10 & Q3b, Q4b with {}
Q11. (~(adj_x(3, 3, d(5, 3, 3)) = 3));

Q9 & F4 with {x_f4/3, y_f4/3, d_f4/d(5, 3, 3)}
Q12. ((adj_x(3, 3, d(5, 3, 3)) = 3));

Q11 & Q12 with {}
Q13. ()
Contradiction reached! Discard Q0-Q13.
P5. (Wumpus(5, 4, 3))